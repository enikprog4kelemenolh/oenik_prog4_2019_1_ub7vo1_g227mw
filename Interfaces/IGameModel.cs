interface IGameModel
{
	int width;
	int height;

	List<GameObject> GameObjects {get;}	

	void Turn();
	string Serialize();
	void HandleInput();
	static IGameModel DeSerialize(string data);
}