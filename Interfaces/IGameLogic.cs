interface IGameLogic

{
	IGameModel GameModel{get;}

	IGameModel StartGame();
	void SaveGame();	
	IGameModel LoadGame(string saveGamePath);
	void Exit();
	void ShowHighScores();
	List<string> GetSaveGameList();
}