var hierarchy =
[
    [ "Application", null, [
      [ "Frogger.App", "class_frogger_1_1_app.html", null ],
      [ "Frogger.App", "class_frogger_1_1_app.html", null ],
      [ "Frogger.App", "class_frogger_1_1_app.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "Frogger.Controls.GameControl", "class_frogger_1_1_controls_1_1_game_control.html", null ]
    ] ],
    [ "Frogger.DisplayComponents.GameDisplay", "class_frogger_1_1_display_components_1_1_game_display.html", null ],
    [ "GameLogic.GLogic", "class_game_logic_1_1_g_logic.html", null ],
    [ "GameModel.GModel", "class_game_model_1_1_g_model.html", null ],
    [ "GameModel.Elements.HitBox", "class_game_model_1_1_elements_1_1_hit_box.html", [
      [ "GameModel.Elements.Enemy", "class_game_model_1_1_elements_1_1_enemy.html", null ],
      [ "GameModel.Elements.Frog", "class_game_model_1_1_elements_1_1_frog.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "Frogger.MainWindow", "class_frogger_1_1_main_window.html", null ],
      [ "Frogger.MainWindow", "class_frogger_1_1_main_window.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Window", null, [
      [ "Frogger.MainWindow", "class_frogger_1_1_main_window.html", null ],
      [ "Frogger.MainWindow", "class_frogger_1_1_main_window.html", null ],
      [ "Frogger.MainWindow", "class_frogger_1_1_main_window.html", null ]
    ] ]
];