var class_game_logic_1_1_g_logic =
[
    [ "GLogic", "class_game_logic_1_1_g_logic.html#a10df9437573cbef4b33212c5960f84ea", null ],
    [ "Counter", "class_game_logic_1_1_g_logic.html#aa9dc3d1e0755c753c9d6e35e0b4258d6", null ],
    [ "EnemyAdd", "class_game_logic_1_1_g_logic.html#a3fe51f784f7715ee5ef2f030130c585e", null ],
    [ "EnemyInMove", "class_game_logic_1_1_g_logic.html#aa2b1fbd97342256ad9eece2b03527e68", null ],
    [ "EnemyMove", "class_game_logic_1_1_g_logic.html#af57b47ebf0032b5cd5956b9ed71cd39e", null ],
    [ "FrogJumps", "class_game_logic_1_1_g_logic.html#a7ed351003990a0695a4ce14f5feb18ff", null ],
    [ "RipFroggy", "class_game_logic_1_1_g_logic.html#a669c868af4d67c1bafcdca0ea60449d1", null ],
    [ "Won", "class_game_logic_1_1_g_logic.html#abf25f5ca6c7b553a256acefa918b34dc", null ],
    [ "ScreenRefresh", "class_game_logic_1_1_g_logic.html#af21b08666f579dd24e7e5551d82751e1", null ]
];