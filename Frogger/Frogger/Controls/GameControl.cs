﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Frogger.Controls
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Frogger.DisplayComponents;
    using GameLogic;
    using GameModel;

    public class GameControl : FrameworkElement
    {
        private GModel gameModel;
        private GLogic gameLogic;
        private GameDisplay gameDisplay;
        private DispatcherTimer timer;

        public GameControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        // Render the graphics
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.gameDisplay != null)
            {
                this.gameDisplay.DrawOnScreen(drawingContext);
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            Window window = Window.GetWindow(this);
            if (window != null)
            {
                this.gameModel = new GModel();
                this.gameLogic = new GLogic(this.gameModel);
                this.gameDisplay = new GameDisplay(this.gameModel);

                this.timer = new DispatcherTimer();
                this.timer.Interval = TimeSpan.FromMilliseconds(25);
                this.timer.Tick += this.Timer_Tick;
                this.timer.Start();

                window.KeyDown += this.Window_KeyDown;
                this.gameLogic.ScreenRefresh += (obj, args) => this.InvalidateVisual();

                this.InvalidateVisual();
            }
        }

        // Key down events for controlling
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.P)
            {
                this.timer.IsEnabled = !this.timer.IsEnabled;
            }
            else if (e.Key == System.Windows.Input.Key.Space)
            {
                Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();
            }

            if (this.timer.IsEnabled)
            {
                if (e.Key == System.Windows.Input.Key.A)
                {
                    this.gameDisplay.Uri = new Uri(@"pack://application:,,,/images/FrogL.png");
                    this.gameLogic.FrogJumps(Directions.Left);
                }
                else if (e.Key == System.Windows.Input.Key.D)
                {
                    this.gameDisplay.Uri = new Uri(@"pack://application:,,,/images/FrogR.png");
                    this.gameLogic.FrogJumps(Directions.Right);
                }
                else if (e.Key == System.Windows.Input.Key.W)
                {
                    this.gameDisplay.Uri = new Uri(@"pack://application:,,,/images/Frog.png");
                    this.gameLogic.FrogJumps(Directions.Up);
                }
                else if (e.Key == System.Windows.Input.Key.S)
                {
                    this.gameDisplay.Uri = new Uri(@"pack://application:,,,/images/FrogD.png");
                    this.gameLogic.FrogJumps(Directions.Down);
                }
            }
        }

        // Move the enemies with timer if timer isEnabled false stop the timer
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.gameLogic.EnemyInMove(this.timer);
            this.gameLogic.Won();
            this.gameLogic.Counter(this.timer);
            if (this.gameModel.WonScore == 5)
            {
                this.timer.IsEnabled = false;
            }

            if (!this.timer.IsEnabled && this.gameModel.WonScore != 5)
            {
                this.gameDisplay.Uri = new Uri(@"pack://application:,,,/images/rip.png");
            }
        }
    }
}
