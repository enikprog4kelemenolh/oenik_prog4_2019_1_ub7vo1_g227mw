﻿// <copyright file="GameDisplay.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Frogger.DisplayComponents
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using GameModel;
    using GameModel.Elements;

    public class GameDisplay
    {
        public GameDisplay(GModel gameModel)
        {
            this.gameModel = gameModel;
            this.coords = new int[] { 100, 280, 460, 640, 810 };
            this.Uri = new Uri(@"pack://application:,,,/images/Frog.png");
        }

        private GModel gameModel;

        private int[] coords;

        public Uri Uri { get; set; }

        // Draw the contents of the game
        public void DrawOnScreen(DrawingContext context)
        {
            DrawingGroup dg = new DrawingGroup();

            // Draw the actual points
            FormattedText score = new FormattedText(
                "Score: " + this.gameModel.Score.ToString(),
                                                     System.Globalization.CultureInfo.CurrentCulture,
                                                     FlowDirection.LeftToRight,
                                                     new Typeface("Comic-Sans"),
                                                     20,
                                                     Brushes.Black);

            dg.Children.Add(new GeometryDrawing(null, new Pen(Brushes.Black, 1), score.BuildGeometry(new Point(100, 840))));

            // Draw the actual frogs
            FormattedText lives = new FormattedText(
                "Frogs: " + this.gameModel.Frog.Frogs.ToString(),
                                                     System.Globalization.CultureInfo.CurrentCulture,
                                                     FlowDirection.LeftToRight,
                                                     new Typeface("Comic-Sans"),
                                                     20,
                                                     Brushes.Black);

            dg.Children.Add(new GeometryDrawing(null, new Pen(Brushes.Black, 1), lives.BuildGeometry(new Point(10, 840))));

            // Draw counter
            FormattedText counter = new FormattedText(
                "Time Left: " + String.Format("{0:0}", this.gameModel.CountDown),
                                                     System.Globalization.CultureInfo.CurrentCulture,
                                                     FlowDirection.LeftToRight,
                                                     new Typeface("Comic-Sans"),
                                                     20,
                                                     Brushes.Black);

            dg.Children.Add(new GeometryDrawing(null, new Pen(Brushes.Black, 1), counter.BuildGeometry(new Point(800, 840))));

            // Draw the enemies
            foreach (var enemy in this.gameModel.Enemies)
            {
                ImageDrawing enemyImg = new ImageDrawing();
                enemyImg.Rect = enemy.Terulet;

                if (enemy.EnemyType == EnemyType.Truck)
                {
                    enemyImg.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/images/truck.png"));
                }
                else if (enemy.EnemyType == EnemyType.Logs)
                {
                    enemyImg.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/images/logs.png"));
                }
                else if (enemy.EnemyType == EnemyType.Taxi)
                {
                    enemyImg.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/images/taxi.png"));
                }
                else if (enemy.EnemyType == EnemyType.Police)
                {
                    enemyImg.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/images/police.png"));
                }
                else if (enemy.EnemyType == EnemyType.Turtle)
                {
                    enemyImg.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/images/turtle.png"));
                }

                dg.Children.Add(enemyImg);
            }

            // Draw frogs on leafs when win a round
            for (int i = 0; i < this.gameModel.WonScore; i++)
            {
                ImageDrawing wonFrog = new ImageDrawing();
                wonFrog.Rect = new Rect(this.coords[i], 20, 60, 60);
                wonFrog.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/images/Frog.png"));
                dg.Children.Add(wonFrog);
            }

            // Draw the Frog
            ImageDrawing frogImg = new ImageDrawing();
            frogImg.Rect = this.gameModel.Frog.Terulet;
            frogImg.ImageSource = new BitmapImage(this.Uri);

            dg.Children.Add(frogImg);
            context.DrawDrawing(dg);
        }
    }
}
