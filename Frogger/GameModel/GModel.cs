﻿// <copyright file="GModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System.Collections.Generic;
    using GameModel.Elements;

    public class GModel
    {
        public GModel()
        {
            int frogWidth = 60;
            int frogHeight = 60;
            this.CountDown = 20;

            // Start position of the frog... middle bottom of the screen
            this.Frog = new Frog((Config.WindowWidth / 2) - (frogWidth / 2), Config.WindowHeight - frogHeight - (frogHeight / 2), frogWidth, frogHeight);
            this.Enemies = new List<Enemy>();
        }

        // Player's score
        public int Score { get; set; }

        // Frog
        public Frog Frog { get; set; }

        // Enemies collection
        public List<Enemy> Enemies { get; set; }

        // Number of frogs on leafs
        public int WonScore { get; set; }

        // Counter starts from 20s
        public double CountDown { get; set; }
    }
}
