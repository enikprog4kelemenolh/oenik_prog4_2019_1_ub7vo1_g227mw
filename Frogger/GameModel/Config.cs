﻿// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    // Default settings of the game
    public static class Config
    {
        private static double windowWidth = 1000;
        private static double windowHeight = 900;

        public static double WindowWidth { get => windowWidth; set => windowWidth = value; }

        public static double WindowHeight { get => windowHeight; set => windowHeight = value; }
    }
}
