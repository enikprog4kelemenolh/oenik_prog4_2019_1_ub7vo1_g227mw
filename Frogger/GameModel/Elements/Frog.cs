﻿// <copyright file="Frog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel.Elements
{
    // Frog character
    public class Frog : HitBox
    {
        public Frog(double x, double y, double szelesseg, double magassag)
           : base(x, y, szelesseg, magassag) => this.Frogs = 3;

        public double Frogs { get; set; }
    }
}
