﻿// <copyright file="Enemy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel.Elements
{
    public enum EnemyType
    {
        Taxi,
        Truck,
        Police,
        Logs,
        Turtle
    }

    public class Enemy : HitBox
    {
        public Enemy(double x, double y, double szelesseg, double magassag, int speed, EnemyType enemyType)
            : base(x, y, szelesseg, magassag, speed)
        {
            this.EnemyType = enemyType;
        }

        public EnemyType EnemyType { get; set; }
    }
}
