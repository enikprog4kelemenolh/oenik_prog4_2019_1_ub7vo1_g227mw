﻿// <copyright file="HitBox.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel.Elements
{
    using System.Windows;

    // Default shape in the game
    public class HitBox
    {
        private Rect terulet;

        public HitBox(double x, double y, double szelesseg, double magassag)
        {
            this.terulet = new Rect(x, y, szelesseg, magassag);
            this.DX = 5;
        }

        public HitBox(double x, double y, double szelesseg, double magassag, int speed)
        {
            this.terulet = new Rect(x, y, szelesseg, magassag);
            this.DX = speed;
        }

        public int DX { get; set; }

        public Rect Terulet
        {
            get { return this.terulet; }
        }

        // Set x,y coordinates of the shape
        public void Set(double x, double y)
        {
            this.terulet.X = x;
            this.terulet.Y = y;
        }

        // Move the shape horizontaly
        public void MoveX(double diff)
        {
            this.terulet.X += diff;
        }

        // Move the shpae on verticaly
        public void MoveY(double diff)
        {
            this.terulet.Y += diff;
        }
    }
}
