﻿// Enable XML documentation output
// <copyright file="GLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Windows.Threading;
    using GameModel;
    using GameModel.Elements;

    public enum Directions
    {
        Up,
        Down,
        Left,
        Right
    }

    public class GLogic
    {
        private GModel gameModel;

        public event EventHandler ScreenRefresh;

        // Return the state of the moving
        private bool fault;

        public GLogic(GModel gameModel)
        {
            this.gameModel = gameModel;
            this.EnemyAdd();
        }

        private static readonly Random Random = new Random();
        private static readonly object SyncLock = new object();

        // Function to get a random number
        private static int RandomNumber(int min, int max)
        {
            lock (SyncLock)
            { // synchronize
                return Random.Next(min, max);
            }
        }

        // Frog jumps with directions
        public void FrogJumps(Directions direction)
        {
            int jumpSizeVertical = 100;
            int jumpSizeHorizontal = 50;

            if (direction == Directions.Left && (this.gameModel.Frog.Terulet.Left - jumpSizeHorizontal) > 0)
            {
                this.gameModel.Frog.MoveX(-jumpSizeHorizontal);
                this.gameModel.Score += 1;
            }

            if (direction == Directions.Right && (this.gameModel.Frog.Terulet.Right + jumpSizeHorizontal) < Config.WindowWidth)
            {
                this.gameModel.Frog.MoveX(jumpSizeHorizontal);
                this.gameModel.Score += 1;
            }

            if (direction == Directions.Up && (this.gameModel.Frog.Terulet.Top - jumpSizeVertical) > 0)
            {
                this.gameModel.Frog.MoveY(-jumpSizeVertical);
                this.gameModel.Score += 1;
            }

            if (direction == Directions.Down && (this.gameModel.Frog.Terulet.Bottom + jumpSizeVertical) < Config.WindowHeight)
            {
                this.gameModel.Frog.MoveY(jumpSizeVertical);
                this.gameModel.Score += 1;
            }

            this.ScreenUpdate();
        }

        // Enemy moving with deafult speed or with the modified speed
        public void EnemyMove(HitBox enemy)
        {
                enemy.MoveX(-enemy.DX);
                if (enemy.Terulet.Right <= 0)
                {
                    enemy.Set(Config.WindowWidth, enemy.Terulet.Y);
                }

            this.ScreenUpdate();
        }

        // Add enemies on road and river
        public void EnemyAdd()
        {
            for (int i = 1; i < 4; i++)
            {
                int startPosX = RandomNumber(0, 1001);
                int actualPosX = startPosX;
                int def = RandomNumber(0, 300);
                while (startPosX + 1000 + def >= actualPosX)
                {
                    EnemyType type = EnemyType.Logs;
                    int distance = RandomNumber(80, 200);
                    if (i % 2 == 0)
                    {
                        type = EnemyType.Turtle;
                        distance = RandomNumber(80, 300);
                    }

                    int speed = 0;
                    int width = 0;
                    switch (type)
                    {
                        case EnemyType.Logs:
                            width = 300;
                            speed = 5;
                            break;
                        case EnemyType.Turtle:
                            width = 80;
                            speed = 7;
                            break;
                        default:
                            width = 0;
                            break;
                    }

                    if (startPosX + 1000 + def >= actualPosX + width + distance)
                    {
                        this.gameModel.Enemies.Add(new Enemy(actualPosX, i * 100, width, 80, speed, type));
                    }

                    actualPosX += width + distance;
                }
            }

            for (int i = 5; i < 8; i++)
            {
                int startPosX = RandomNumber(0, 1001);
                int actualPosX = startPosX;
                int def = RandomNumber(0, 150);
                while (startPosX + 1000 + def >= actualPosX)
                {
                    EnemyType type = (EnemyType)i - 5;
                    int width = 0;
                    int speed = 0;
                    switch (type)
                    {
                        case EnemyType.Truck:
                            width = 300;
                            speed = 5;
                            break;
                        case EnemyType.Taxi:
                            width = 150;
                            speed = 9;
                            break;
                        case EnemyType.Police:
                            width = 150;
                            speed = 13;
                            break;
                        default:
                            width = 0;
                            break;
                    }

                    int distance = RandomNumber(70, 120);
                    if (startPosX + 1000 + def >= actualPosX + width + distance)
                    {
                        this.gameModel.Enemies.Add(new Enemy(actualPosX, i * 100, width, 80, speed, type));
                    }

                    actualPosX += width + distance;
                }
            }
        }

        // Return the fault moving => dead frog
        public bool RipFroggy(List<Enemy> enemies)
        {
            this.fault = false;
            if (this.gameModel.Frog.Terulet.Top <= 310 && this.gameModel.Frog.Terulet.Top > 10 && this.gameModel.Frog.Terulet.Left > 0 && this.gameModel.Frog.Terulet.Right < Config.WindowWidth)
            {
                int i = 0;
                bool frogDie = true;
                while (i < enemies.Count && frogDie)
                {
                    if (this.FrogOnLogs(enemies[i]))
                    {
                        return false;
                    }

                    i++;
                }

                return frogDie;
            }

            foreach (var enemy in enemies)
            {
                if (enemy.EnemyType == EnemyType.Truck || enemy.EnemyType == EnemyType.Taxi || enemy.EnemyType == EnemyType.Police)
                {
                    if (enemy.Terulet.Top + 10 == this.gameModel.Frog.Terulet.Top)
                    {
                        if (enemy.Terulet.Left + 15 < this.gameModel.Frog.Terulet.Right && enemy.Terulet.Right > this.gameModel.Frog.Terulet.Right)
                        {
                            return true;
                        }
                    }
                }
            }

            return this.fault;
        }

        // Return the number of winning game
        public void Won()
        {
            if (this.gameModel.Frog.Terulet.Top == 10)
            {
                this.gameModel.WonScore++;
                this.gameModel.Score += Convert.ToInt16(this.gameModel.CountDown * 2);
                this.gameModel.Frog.Frogs++;
                this.gameModel.Score += 5;
                if (this.gameModel.WonScore == 5)
                {
                    this.gameModel.Score += 25;
                }

                Thread.Sleep(1000);
                this.gameModel.CountDown = 20;
                this.Respawn();
            }
        }

        // Check the enemies moving => frog on logs? if not truck hit the frog?
        public void EnemyInMove(DispatcherTimer timer)
        {
            foreach (var enemy in this.gameModel.Enemies)
            {
                if (this.FrogOnLogs(enemy))
                {
                    this.gameModel.Frog.MoveX(-this.gameModel.Frog.DX);
                    if (this.gameModel.Frog.Terulet.Right <= 0)
                    {
                        this.gameModel.Frog.Set(Config.WindowWidth, this.gameModel.Frog.Terulet.Y);
                    }
                }

                this.EnemyMove(enemy);
            }

            if (this.RipFroggy(this.gameModel.Enemies))
            {
                this.gameModel.Frog.Frogs--;
                if (this.gameModel.Frog.Frogs != 0)
                {
                    this.Respawn();
                }
                else
                {
                    timer.Stop();
                }
            }

            this.ScreenUpdate();
        }

        // Time Left module
        public void Counter(DispatcherTimer timer)
        {
            this.gameModel.CountDown -= timer.Interval.TotalSeconds;
            if (this.gameModel.CountDown <= 0)
            {
                this.gameModel.Frog.Frogs--;
                this.gameModel.CountDown = 20;
                this.Respawn();
                if (this.gameModel.Frog.Frogs == 0)
                {
                    timer.Stop();
                }
            }
        }

        // Update the screen after modification
        private void ScreenUpdate()
        {
            this.ScreenRefresh?.Invoke(this, EventArgs.Empty);
        }

        // After frig died repsawn on default location
        private void Respawn() => this.gameModel.Frog.Set((Config.WindowWidth / 2) - (60 / 2), Config.WindowHeight - 60 - (60 / 2));

        // Frog on logs or not
        private bool FrogOnLogs(Enemy enemy)
        {
            if (enemy.EnemyType == EnemyType.Logs || enemy.EnemyType == EnemyType.Turtle)
            {
                if (enemy.Terulet.Top + 10 == this.gameModel.Frog.Terulet.Top)
                {
                    if (enemy.Terulet.Left + 30 < this.gameModel.Frog.Terulet.Right && enemy.Terulet.Right - 30 > this.gameModel.Frog.Terulet.Left)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
